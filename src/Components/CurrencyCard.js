import React, {Component} from 'react';
import {Button, Animated, View, Image, ScrollView, TouchableHighlight, Text,} from "react-native";
import textStyles, {deviceWidth} from "../theme/textStyles";
import AppColors from "../theme/AppColors";
import {withNavigation} from "react-navigation";
/*
import {Header} from "../comp/Header";
*/
import { connect } from 'react-redux'
import {toggleFavorite} from "../Reducers/cryptoReducer";
import {ios} from "../theme/AppStyles";

class CurrencyCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFavorite: false,
            show: true,
        };
    }

    render() {
        const {item, toggleFavorite} = this.props;
        const {isFavorite} = this.state;
        const {value, label} = item;

        /**Graphic max's width defaults to 80% of phone's max width*/
        const maxWidth= deviceWidth*.8;

        return (
            <View style={{flexDirection: 'row', width: '95%', backgroundColor: AppColors.white, borderWidth: 3,
                borderColor: isFavorite ? AppColors.red1t2 : AppColors.grey2t15, borderRadius: 20, paddingVertical:20, alignItems: 'center', margin: 5}}>
                {/**Main row*/}
                <View style={{flexDirection: 'row', width: deviceWidth*.95, alignItems: 'center', top: -8, marginLeft: 10}}>
                    {/**Favorite button*/}
                    <TouchableHighlight underlayColor={'transparent'}
                                        style={{position: 'absolute', right: 20}}
                                        onPress={()=>this.setState({isFavorite: !isFavorite},
                                            ()=>toggleFavorite(item, isFavorite))}>
                        <Image source={require('../../img/heart.png')}
                               style={{height: 20,  width: 20, marginHorizontal: 10, tintColor: isFavorite? AppColors.red1 : AppColors.black}}/>
                    </TouchableHighlight>
                    <Image source={require('../../img/bitcoin.png')} style={{height: 26,  width: 26, tintColor: AppColors.yellow1}}/>

                    <Text style={[textStyles.title1, {width: 'auto', padding: 5, borderRadius: 10, borderColor: AppColors.grey2, borderWidth: 1, textAlign: 'center', marginLeft: 10, color: AppColors.black, paddingHorizontal: 5}]}>{item.symbol}</Text>

                    {/**Col 1*/}
                    <View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[textStyles.body3, {color: AppColors.black, paddingHorizontal: 10}]}>24h: </Text>
                            <Text style={[textStyles.caption1, {color: item.priceChange > 0 ? AppColors.green1 : AppColors.red1}]}>{Number(item.priceChange).toFixed(2)} $ ({Number(item.priceChangePercent).toFixed(1)}% )</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[textStyles.caption1, {color: AppColors.black, paddingHorizontal: 10}]}>Price: </Text>
                            <Text style={[textStyles.body3, {color:  AppColors.black, fontWeight: 'bold'}]}>{Number(item.lastPrice).toFixed(2)} $</Text>
                        </View>
                    </View>

                    {/**Col 1*/}
                    {!ios && <View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[textStyles.body3, {color: AppColors.black, paddingHorizontal: 6}]}>Last: </Text>
                            <Text style={[textStyles.caption1]}>{Number(item.lastPrice).toFixed(2)} $</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[textStyles.caption1, {color: AppColors.black, paddingHorizontal: 6}]}>bid: </Text>
                            <Text style={[textStyles.body3, {color:  AppColors.black, fontWeight: 'bold'}]}>{Number(item.bidQty).toFixed(2)} $</Text>
                        </View>
                    </View>}


                </View>
                <Text style={[textStyles.caption1, {color: AppColors.black, paddingHorizontal: 10}]}>{JSON.stringify(item)}</Text>

                {/**Graphic*/}
                <View style={{width: value*maxWidth/2, height: 4, borderRadius: 20, maxWidth: maxWidth, backgroundColor: isFavorite ? AppColors.red1 : AppColors.yellow2, left: 15, position: 'absolute', bottom: 10}}/>
                {/*
                <View style={{width: maxWidth/2, height: 2, borderRadius: 20, maxWidth: maxWidth, backgroundColor: AppColors.yellow2, left: 15, position: 'absolute', bottom: 10}}/>
*/}
                {/*</View>
            <View>*/}
                {/* </View>*/}
            </View>);
    }
}

const mapStateToProps = state => {
    return ({
        favList: state.favList,
    });
};
const mapDispatchToProps = {
    toggleFavorite,
};

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyCard);
