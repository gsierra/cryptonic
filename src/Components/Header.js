import React, {Component} from 'react';
import {Button, Animated, View, Image, ScrollView, TouchableHighlight, Text,} from "react-native";
import textStyles, {deviceWidth} from "../theme/textStyles";
import AppColors from "../theme/AppColors";
import {withNavigation} from "react-navigation";
import {isIPhoneX} from "../theme/AppStyles";
/*
import {Header} from "../comp/Header";
*/

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openDrawer: false,
        };
    }

    render() {
        const {title} = this.props;

        return (<View>
                <View style={{flexDirection: 'row', width: deviceWidth, backgroundColor: AppColors.black, mnarginTop: isIPhoneX ? 60 : 0 , paddingVertical:20, alignItems: 'center'}}>
                    <TouchableHighlight onPress={()=>this.props.navigation.toggleDrawer()}>
                        <Image source={require('../../img/menu.png')} style={{height: 25,  width: 25, marginHorizontal: 10}}/>
                    </TouchableHighlight>
                    <Text style={[textStyles.mainTitle, {color: AppColors.white, paddingHorizontal: 20}]}>{title}</Text>
                    <Image source={require('../../img/bitcoin.png')} style={{height: 26,  width: 26, tintColor: AppColors.white}}/>
                    <Image source={require('../../img/holded-landscape.png')} style={{height: 26,  width: 103, marginHorizontal: 10, right: 20, position: 'absolute'}}/>
                </View>

                {/**Separator*/}
                <View style={{backgroundColor: AppColors.grey2t5, height: 3, width: deviceWidth, top: -2}}/>
            </View>
        );
    }
}

export default withNavigation(HomeScreen);
