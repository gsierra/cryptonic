import Binance from 'binance-api-react-native'
import {Alert} from "react-native";

const client = Binance();


/**First Approach*/
export const fetchTickers = async () => {
    return await client.allBookTickers();
};

export const fetchCrypto = async () => {
    return await client.prices();
};

export const dailyStats = async () => {
    return await client.dailyStats();
};

export const fetchPrices = async (sorted) => {
    const pricesObject = await client.prices();
    const pricesArray = [];
    Object.keys(pricesObject).forEach((key)=>{
        pricesArray.push({label: key, isFavorite: false, value: Number(pricesObject[key])})
    });

    return sorted ?
        pricesArray.sort((a, b) =>
            b.value - a.value) :
        pricesArray;
};

/**Second Approach, more accurate values*/
/*export const fetchTickers = async (sorted) => {
   /!**1. Fetch all tickers*!/
    const pricesObject = await client.allBookTickers();

    console.log("TEST: 1. Obj: "+JSON.stringify(pricesObject));
    /!**2. Convert it into Array in order to Sort
     *  and display in a FlatList rather than Object.map()*!/
    const pricesArray = [];
    Object.keys(pricesObject).forEach((key)=>{
        pricesArray.push({
                label: key,
                symbol: pricesObject[key].symbol,
                value: pricesObject[key].bidPrice,
                bidQty: pricesObject[key].bidQty,
                askPrice: pricesObject[key].askPrice,
                askQty: pricesObject[key].askQty,
                isFavorite: false,
            }
        )
    });
    console.log("TEST: 2. Arr: "+JSON.stringify(pricesArray));

    /!**3. Store the whole list into the Store*!/
   return sorted ?
        pricesArray.sort((a, b) =>
            b.priceChangePercent - a.priceChangePercent) :
        pricesArray
};*/

/**Third Approach, will return all tickers and is resource-expensive*/
/*export const dailyStats = async (sorted) => {
    const dailyStatsObj = await client.dailyStats();
    /!*console.error(pricesArray);*!/

    /!*Alert.alert("SRES", JSON.stringify(pricesObject));*!/

    const dailyStatsArr = [];
    Object.keys(dailyStatsObj).forEach((key)=>{
        dailyStatsArr.push({
                label: key,
                symbol: dailyStatsObj[key].symbol, //redundant
                value: dailyStatsObj[key].bidPrice,
                priceChange: dailyStatsObj[key].priceChange,
                priceChangePercent: dailyStatsObj[key].priceChangePercent,
                bidQty: dailyStatsObj[key].bidQty,
                askPrice: dailyStatsObj[key].askPrice,
                askQty: dailyStatsObj[key].askQty,
                isFavorite: false,
            }
        )
    });

    return sorted ?
        dailyStatsArr.sort((a, b) =>
            b.priceChangePercent - a.priceChangePercent).slice(0,10) :
        dailyStatsArr.slice(0, 50);
};*/

/**Third Approach, will return all tickers and is resource-expensive*/
export const exchangeInfo = async () => {
    const dailyStatsObj = await client.exchangeInfo();
    /*console.error(pricesArray);*/

    Alert.alert("SRES", JSON.stringify(dailyStatsObj.symbols[4]));
};
