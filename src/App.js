import React, {Component} from 'react';
import {StatusBar, View, ActivityIndicator} from 'react-native';
import AppContainer from "./Routes";
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';

import store from "./store";

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        /**Initial Setup*/
        /*StatusBar.setHidden(true, true);*/
        StatusBar.setBackgroundColor('#000');
        StatusBar.setHidden(false, true);
        StatusBar.setBarStyle("light-content");
        console.disableYellowBox = true;
    }

    render() {
        return (<Provider store={store}>
            {/*<PersistGate loading={<ActivityIndicator/>}
                         persistor={persistor}>*/}
                <AppContainer uriPrefix="/app" />
           {/* </PersistGate>*/}
        </Provider>)
    }
}
