import { createDrawerNavigator, createAppContainer } from "react-navigation";

import {Animated, Text, View, Image, Button, StyleSheet} from "react-native";
import React, {Component} from "react";
import HomeScreen from "./screens/HomeScreen";
import Favorites from "./screens/Favorites";
import TopGainers from "./screens/TopGainers";

const MyDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            drawerLabel: 'Crypto Pairs',
            drawerIcon: ({ tintColor }) => (
                <Image
                    resizeMode={'cover'}
                    source={require('../img/bitcoin.png')}
                    style={[{width: 30, height: 30}]}
                />
            ),
        }
    },  Favorites: {
        screen: Favorites,
        navigationOptions: {
            drawerLabel: 'Favorites',
            drawerIcon: ({ tintColor }) => (
                <Image
                    source={require('../img/heart.png')}
                    style={[{tintColor: tintColor, width: 30, height: 30}]}
                />
            ),
        }
    },  TopGainers: {
        screen: TopGainers,
        navigationOptions: {
            drawerLabel: 'TopGainers',
            drawerIcon: ({ tintColor }) => (
                <Image
                    resizeMode={'contain'}
                    source={require('../img/fire.png')}
                    style={[{tintColor: tintColor, width: 30, height: 30}]}
                />
            ),
        }
    },
});

const AppContainer = createAppContainer(MyDrawerNavigator);

export default AppContainer;
