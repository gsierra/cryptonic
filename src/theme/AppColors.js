
export default {

    /**Green*/
    green1: "#47cc8c",
    green2: "#61fa9e",
    green3: "#22563c",

    /*Degradate*/
    greenDeg: [this.green1, this.green2],

    /*Transp*/
    green1t2: "rgba(71, 204, 140, 0.2)",
    green1t5: "rgba(71, 204, 140, 0.5)",
    green1t8: "rgba(71, 204, 140, 0.8)",

    green3t7: "rgba(34, 86, 60, 0.7)",


    /**Red*/
    red1: "#FA5868",
    red2: "#FD8669",
    red3: "#7C2E39",
    /*Transp*/
    red3t7: "rgba(124, 46, 57, 0.7)", //Desayuno
    red1t2: "rgba(250, 88, 104, 0.2)", //Desayuno

    /*Degradate*/
    redDeg: [this.red1, this.red2],


    /**Yellow*/
    yellow1: "#f3b742",
    yellow2: "#fae373",
    yellow3: "#8c461f",

    yellow3t7: "rgba(140, 70, 31, 0.7)",


    /**Blue*/
    blue1: "#0ab2fa",
    blue2: "#87ebfa",
    blue3: "#085E7C",

    /*Transp*/
    blue3t7: "rgba(8, 94, 124, 0.7)",

    /**Grey*/
    grey1: "#9b9b9b",
    grey2: "#d8d8d8",
    grey3: "#333232",

    /*Transp*/
    grey1t15: "rgba(155, 155, 155, 0.15)",
    grey2t15: "rgba(216, 216, 216, 0.15)",
    grey2t2: "rgba(216, 216, 216, 0.2)",
    grey2t4: "rgba(216, 216, 216, 0.4)",
    grey2t5: "rgba(216, 216, 216, 0.5)",
    grey2t3: "rgba(216, 216, 216, 0.3)",

    //Tips
    greyDeg1: "#eee",
    greyDeg2: "#ddd",

    /*ToBe defined*/

    brandInfo: "#62B1F6",
    brandSuccess: "#5cb85c",
    brandDanger: "#d9534f",
    brandWarning: "#f0ad4e",

    paleBlack: "#222",
    black: "#000",
    white: "#fff",
    brandDark: "#000",
    brandLight: "#f4f4f4",
    brandWhite: '#fff',
    gold: "#bf9b30",


    /*transp*/
    cardDark: "rgba(0, 0, 0, 0.6)",
    darkLessTransp: "rgba(0, 0, 0, 0.4)",
    darkTransp: "rgba(0, 0, 0, 0.5)",
    darkyTransp: "rgba(0, 0, 0, 0.11)",
    darkerTransp: "rgba(8, 94, 124, 0.8)",
    whiteTransp: "rgba(255, 255, 255, 0.5)",
    whiteVeryTransp: "rgba(255, 255, 255, 0.35)",

};


