
import AppColors from "./AppColors"
import {Dimensions} from "react-native";

export const sizeHeadline = 22;
export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;
export const ios = Platform.OS === 'ios';
export const isIPhoneX = ios && deviceHeight >= 812;


export default {
    /**Filter*/
    filterBtnWrapper: {
        backgroundColor: AppColors.white,
        position: 'absolute',
        zIndex: 100,
        top: 0,
        right: 0,
        borderWidth: 3,
        borderColor: AppColors.grey2t15,
        borderRadius: 20,
        paddingHorizontal: 3,
       /* width: deviceWidth*.38,*/
    },
    filterBtn: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 3,
        alignSelf: 'center',
    },

    /**Headline*/


};
