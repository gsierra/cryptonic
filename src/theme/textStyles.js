
import AppColors from "./AppColors"
import {Dimensions} from "react-native";

export const sizeHeadline = 22;
export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;
export const ios = Platform.OS === 'ios';

export default {
    /**Title*/
    mainTitle: {
        fontWeight: '600',
        fontSize: 25,
        color: AppColors.black
    },

    /**Headline*/
    headline: {

        fontSize: sizeHeadline,
        marginVertical: sizeHeadline,
        textAlign: "center",
        letterSpacing: 0.5,
        color: AppColors.black
    },
    headlineWhite: {

        fontSize: sizeHeadline,
        marginVertical: sizeHeadline,
        textAlign: "center",
        letterSpacing: 0.5,
        color: AppColors.white
    },


    /**Subhead*/
    subhead: {

        fontSize: 18,
        color: AppColors.black
    },

    /**Title*/
    title1: {
        fontWeight: '600',
        fontSize: 16,
        color: AppColors.black
    },

    title2: {
        fontWeight: '600',

        fontSize: 15,
        color: AppColors.black
    },

    title3: {

        fontSize: 14,
        color: AppColors.black
    },

    title4: {

        fontSize: 12,
        color: AppColors.black
    },

    title5: {

        fontSize: 10,
        color: AppColors.black
    },


    /**Body*/
    body1: {

        fontSize: 16,
        color: AppColors.black
    },

    body2: {

        fontSize: 14,
        color: AppColors.black
    },

    body3: {

        fontSize: 12,
        color: AppColors.black
    },


    body4: {

        fontSize: 10,
        color: AppColors.black
    },

    body5: {

        fontSize: 9,
        color: AppColors.black
    },

    body6: {

        fontSize: 9,
        color: AppColors.black
    },

    caption1: {

        fontSize: 12,
        color: AppColors.black
    },

    caption2: {

        fontSize: 10,
        color: AppColors.black
    },

    caption3: {
        fontSize: 10,
        color: AppColors.black
    },

};
