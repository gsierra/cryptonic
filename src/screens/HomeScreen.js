import React, {Component} from 'react';
import {
    Alert,
    FlatList,
    View,
    RefreshControl,
    SafeAreaView,
    Text,
    ScrollView,
    ActivityIndicator,
    Picker,
    TouchableHighlight,
    Image,
} from "react-native";
import NetInfo from "@react-native-community/netinfo";
import Header from "../Components/Header";
import textStyles, {deviceHeight, deviceWidth} from "../theme/textStyles";
import _ from "lodash";
import AppColors from "../theme/AppColors";
import CurrencyCard from "../Components/CurrencyCard";
import AppStyles, {ios} from "../theme/AppStyles";
import {GRAPH_HEIGHT, GRAPH_SEPARATION_LINES} from "../constants/constants";
import { connect } from 'react-redux';
import {getSortedCrypto} from "../store";

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sorted: true, //defaults to sorted list
            zoom: 1,
            connected: false,
        };
        this._onRefresh=this._onRefresh.bind(this);

        /**Subscribe to connection changes*/
        this.unsuscribeConnection = NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
            if(state.isConnected)
                this.setState({isConnected: true},
                    this._onRefresh);
            else
                Alert.alert("Sin conexión", "Vaya parece que no tienes conexión...",
                    [{text: '¡Reintentar!', onPress: async () => {}}]);
        });
    }

    async componentDidMount(): void {
        if(NetInfo.isConnected)
            await this._onRefresh();
    }

    componentWillUnmount(): void {
        this.unsuscribeConnection();
    }

    async _onRefresh(){
        await this.props.getSortedCrypto(this.state.sorted, false);
    }

    renderGridLine = (separation) => {return(<View style={{flexDirection: 'row',/* top: -20*separation,*/ height: GRAPH_HEIGHT/GRAPH_SEPARATION_LINES}}>
        <Text style={[textStyles.body5, {color: AppColors.grey1, top: -5, marginHorizontal: 5}]}>{((GRAPH_SEPARATION_LINES-separation)/this.state.zoom).toFixed(1)}</Text>
        <View style={{backgroundColor: AppColors.grey2t5,
            height: 1, width: deviceWidth*.95}}/>
    </View>)};

    render() {
        const {sorted, zoom} = this.state;
        const {loading, cryptoList, favList, getSortedCrypto} = this.props;

        /**Print chart lines*/
        let gridLines = [];
        for(let i=0; i<=GRAPH_SEPARATION_LINES; i++)
            gridLines.push(this.renderGridLine(i));

        return (<SafeAreaView style={{backgroundColor: AppColors.black}}>
                <Header title={'Cryptonic'}/>
                <ScrollView style={{paddingVertical: 15, backgroundColor: AppColors.white, minHeight: deviceHeight}} stickyHeaderIndices={0}
                            refreshControl={<RefreshControl refreshing={loading}
                                                            onRefresh={this._onRefresh}/>} >


                     {/**Filters*/}
                    {!ios && <TouchableHighlight underlayColor={'transparent'}
                                        style={AppStyles.filterBtnWrapper}
                                        onPress={()=>this.setState({sorted: !sorted}, ()=>getSortedCrypto(sorted))}>
                        <View  style={AppStyles.filterBtn}>
                            <Text style={[sorted ? textStyles.caption1 : textStyles.title1, {color: sorted ? AppColors.grey1 : AppColors.yellow1, paddingHorizontal: 10}]}>Popularity</Text>

                            {/**Toggle button*/}

                            <Image source={sorted ?
                                require('../../img/sort.png') :
                                require('../../img/fire.png')}
                                   style={{height: 20,  width: 20, marginVertical: 10}}/>
                            {/*</TouchableHighlight>*/}

                            <Text style={[!sorted ? textStyles.caption1 : textStyles.title1, {color: !sorted ? AppColors.grey1 : AppColors.yellow1, paddingHorizontal: 10}]}>Price</Text>
                        </View>
                    </TouchableHighlight>}


                    {/**Titles*/}
                    <Text style={[textStyles.mainTitle, {paddingHorizontal: 15}]}>Crypto pairs {!_.isEmpty(cryptoList) && '('+cryptoList.length+')'}</Text>
                    <Text style={[textStyles.body1, {padding: 15, marginBottom: 15}]}>Cryptocurrencies distribution sorted by {sorted ? 'price' : 'popularity'}</Text>


                    {/**picker*/}
                    { !ios && !_.isEmpty(cryptoList) && <View style={{width: 150, backgroundColor: AppColors.white, alignSelf: 'center', borderWidth: 1,
                        borderRadius: 20, flexDirection: 'row', alignItems: 'center', top: -20,
                        justifyContent: 'center', borderColor: AppColors.grey2}}>
                        <Text style={[textStyles.body1, {paddingHorizontal: 15, top: 6, marginBottom: 15, marginLeft: 5}]}>Zoom: </Text>

                        <Picker
                            selectedValue={zoom}
                            style={{height: 20, width: 90 }}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({zoom: itemValue})
                            }>
                            <Picker.Item label="1x" value={1} />
                            <Picker.Item label="2x" value={2} />
                            <Picker.Item label="3x" value={3} />
                            <Picker.Item label="4x" value={4} />
                            <Picker.Item label="5x" value={5} />
                        </Picker>
                    </View>}

                    {!_.isEmpty(cryptoList) && <Text style={[textStyles.body5, {padding: 15, color: AppColors.grey1, textAlign: 'center'}]}> Some values might be truncated above this line ( {(GRAPH_SEPARATION_LINES/zoom).toFixed(1)} $) </Text>}

                    {/**Currecny price distribution chart*/}
                    {_.isEmpty(cryptoList) ? <View style={{height: GRAPH_HEIGHT}}>
                        <ActivityIndicator/>
                        <Text style={[textStyles.title2, {textAlign: 'center', margin: 20}]}>Gathering currency pairs chart...</Text>
                        </View>:
                        <FlatList data={ cryptoList}
                                  onEndReached={()=>{/*Todo implement pagination*/}}
                                  style={{marginLeft: 25}}
                                  horizontal={true}
                                  initialNumToRender={10}
                            /* updateCellsBatchingPeriod={10}*/
                                  maxToRenderPerBatch={10}
                                  renderItem={({item}) =><View style={{justifyContent: 'flex-end', maxHeight: GRAPH_HEIGHT, width: zoom*3, alignItems: 'center'}}>
                                      <View style={{justifyContent: 'flex-end',  maxHeight: GRAPH_HEIGHT}}>
                                          {/**Unfortunately, no way to keep it readable*/}
                                          {sorted && zoom > 3 && <Text style={[textStyles.body6, { fontSize: 8, width: 12, color: AppColors.grey1, left: -2}]}>{item.value.toFixed(1)}</Text>}

                                          {/**Associates { val = 1 } to 1 grid separation line (GRAPH_HEIGHT/GRAPH_SEPARATION_LINES:

                                           1   < -- >  GRAPH_HEIGHT/GRAPH_SEPARATION_LINES

                                           */}
                                          <View style={{height: (item.priceChangePercent)*GRAPH_HEIGHT/100, maxHeight: GRAPH_HEIGHT, width: 2, backgroundColor: AppColors.yellow2}}/>
                                      </View>
                                  </View>}
                        />}

                    {/**Grid lines, ToDo improvement would be a flatList with 'top: -20*index' rather than {[-100,-80,-60...]}  */}
                    {!_.isEmpty(cryptoList) &&
                    <View style={{top: -GRAPH_HEIGHT}}>
                        {gridLines}
                    </View>}

                    <Text style={[textStyles.body1, {padding: 15, top: _.isEmpty(cryptoList) ? 0 : -GRAPH_HEIGHT}]}>List of Cryptocurrencies pairs sorted by {sorted ? 'price' : 'popularity'}</Text>

                    {_.isEmpty(cryptoList) ?
                        <Text style={[textStyles.title2, {textAlign: 'center', margin: 20}]}>Gathering currency pairs info...</Text> :
                        <FlatList data={ cryptoList}
                                  style={{top: -GRAPH_HEIGHT}}
                                  initialNumToRender={5}
                                  refreshing={loading}
                            /*onEndReachedThreshold={0.5}
                            onEndReached={()=>this.props.getNextSortedCrypto()}*/
                                  updateCellsBatchingPeriod={10}
                                  maxToRenderPerBatch={10}
                                  renderItem={({item}) =><CurrencyCard
                                      item={item}/>}
                        />}

                </ScrollView>
            </SafeAreaView>

        );
    }
}

const mapStateToProps = state => ({
    cryptoList: state.cryptoList,
    cryptoFullList: state.cryptoFullList,
    page: state.page,
    errorMessage: state.errorMessage,
    favList: state.favList,
    loading: state.loading,
});

const mapDispatchToProps = {
    getSortedCrypto,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
