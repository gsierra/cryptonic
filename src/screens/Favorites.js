import React, {Component} from 'react';
import {Button, Animated, FlatList, SafeAreaView, ScrollView, TouchableHighlight, Text} from "react-native";
import Header from "../Components/Header";
import _ from "lodash";
import textStyles from "../theme/textStyles";
import CurrencyCard from "../Components/CurrencyCard";
import { connect } from 'react-redux';
import AppColors from "../theme/AppColors";

class Favorites extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {favList} = this.props;

        return (<SafeAreaView style={{backgroundColor: AppColors.black}}>
            <ScrollView style={{backgroundColor: AppColors.white}}>
            <Header title={'Favorites'}/>
                {/**Titles*/}
                <Text style={[textStyles.mainTitle, {paddingHorizontal: 15}]}>Favorite Crypto pairs {!_.isEmpty(favList) && '('+favList.length+')'}</Text>
                <Text style={[textStyles.body1, {padding: 15, marginBottom: 15}]}>List of your favorite crypto pairs</Text>

                {_.isEmpty(favList) ?
                    <Text style={[textStyles.title2, {textAlign: 'center', margin: 20}]}>You still have no favorite Crpyto Pairs...</Text> :
                    <FlatList data={ favList}
                              initialNumToRender={5}
                              updateCellsBatchingPeriod={10}
                              maxToRenderPerBatch={10}
                              renderItem={({item}) =><CurrencyCard
                                  item={item}/>}
                    />}
        </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = state => {
    return ({
        favList: state.favList,
    });
};


export default connect(mapStateToProps)(Favorites)
