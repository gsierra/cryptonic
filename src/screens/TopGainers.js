import React, {Component} from 'react';
import {
    Alert,
    FlatList,
    View,
    RefreshControl,
    SafeAreaView,
    Text,
    ScrollView,
    Picker,
    TouchableHighlight,
    Image,
} from "react-native";
import NetInfo from "@react-native-community/netinfo";
import Header from "../Components/Header";
import textStyles, {deviceWidth} from "../theme/textStyles";
import _ from "lodash";
import AppColors from "../theme/AppColors";
import CurrencyCard from "../Components/CurrencyCard";
import AppStyles from "../theme/AppStyles";
import {GRAPH_HEIGHT, GRAPH_SEPARATION_LINES} from "../constants/constants";
import { connect } from 'react-redux';
import {getSortedCrypto} from "../store";

class TopGainers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sorted: true, //defaults to sorted list
            zoom: 1,
            connected: false,
        };
        this._onRefresh=this._onRefresh.bind(this);

        /**Subscribe to connection changes*/
        this.unsuscribeConnection = NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
            if(state.isConnected)
                this.setState({isConnected: true},
                    this._onRefresh);
            else
                Alert.alert("Sin conexión", "Vaya parece que no tienes conexión...",
                    [{text: '¡Reintentar!', onPress: async () => {}}]);
        });
    }

    async componentDidMount(): void {
        if(NetInfo.isConnected)
            await this._onRefresh();
    }

    componentWillUnmount(): void {
        this.unsuscribeConnection();
    }

    async _onRefresh(){
        await this.props.getSortedCrypto(true, true);
    }

    render() {
        const {sorted, zoom} = this.state;
        const {loading, cryptoList} = this.props;

        return (<SafeAreaView style={{backgroundColor: AppColors.black}}>
                <Header title={'Top Gainers'}/>
                <ScrollView style={{paddingVertical: 15, backgroundColor: AppColors.white}} stickyHeaderIndices={0}
                            refreshControl={<RefreshControl refreshing={loading}
                                                            onRefresh={this._onRefresh}/>} >
                    <Text style={[textStyles.subhead, {padding: 15, fontWeight: 'bold'}]}>Top 10 gainer Cryptocurrencies {!_.isEmpty(cryptoList) && '( Among '+cryptoList.length+')'}</Text>

                    {_.isEmpty(cryptoList) ?
                        <Text style={[textStyles.title2, {textAlign: 'center', margin: 20}]}>Gathering currency pairs info...</Text> :
                        <FlatList data={ cryptoList.slice(0,10)}
                                  initialNumToRender={5}
                                  refreshing={loading}
                                  updateCellsBatchingPeriod={10}
                                  maxToRenderPerBatch={10}
                                  renderItem={({item}) =><CurrencyCard
                                      item={item}/>}
                        />}

                </ScrollView>
            </SafeAreaView>

        );
    }
}

const mapStateToProps = state => ({
    cryptoList: state.cryptoList,
    cryptoFullList: state.cryptoFullList,
    page: state.page,
    errorMessage: state.errorMessage,
    favList: state.favList,
    loading: state.loading,
});

const mapDispatchToProps = {
    getSortedCrypto,
};

export default connect(mapStateToProps, mapDispatchToProps)(TopGainers)
