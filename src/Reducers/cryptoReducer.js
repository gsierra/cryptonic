import {fetchPrices} from "../middleware/api";
import {put, take, call} from 'redux-saga/effects';
import {PAGINATION_BATCH} from "../constants/constants";

export const GET_CRYPTO = 'GET_CRYPTO';
export const GET_CRYPTO_SUCCESS = 'GET_CRYPTO_SUCCESS';
export const GET_CRYPTO_FAIL = 'GET_CRYPTO_FAIL';
export const GET_NEXT_BATCH = 'GET_NEXT_BATCH';
export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE';
export const CRYPTO_UPDATE = 'CRYPTO_UPDATE';

const initialState = {
    //list of crptoPairs
    cryptoList: [],
    favList: [],
    //page for pagination,
    page: 0,
    //Have the loading state indicate if it's done getting data.
    loading: true,
    //Have state for error message for recieving an error.
    errorMessage: '',
};

export default function cryptoReducer(state = initialState, action) {
    switch (action.type) {
        case GET_CRYPTO:
            return {...state, cryptoList: {}, loading: action.loading};

        /**Pagination In order to only get a set of 20 per page*/
        case GET_NEXT_BATCH:
            const indexCrypto = state.page*PAGINATION_BATCH;
            /**Adds the new batch of data*/
            return {...state,
                cryptoList: state.cryptoFullList.slice(indexCrypto,
                    indexCrypto+PAGINATION_BATCH),
                cryptoFullList: state.cryptoFullList,
                loading: action.loading,
                page: state.page++
            };

        case GET_CRYPTO_SUCCESS:
            return {...state, cryptoList: action.payload, loading: action.loading};

        case TOGGLE_FAVORITE:
/*
            console.log("Adding "+JSON.stringify(state));
*/
            let newFavList = state.favList;
            action.payload.isFavorite = action.wasFavorite;
            newFavList.push(action.payload);

            return {...state, favList: newFavList, loading: action.loading};

        case GET_CRYPTO_FAIL:
            return {...state, errorMessage: action.payload, loading: action.loading};

        default:
            return state;
    }
}

/**Actions*/
export const fetchData = (bool) => {
    return {
        type: GET_CRYPTO,
        payload: bool,
    };
};

export const getNextBatch = () => {
/*
    console.log("this has been reached");
*/
    return {
        type: GET_NEXT_BATCH,
        loading: false,
    };
};

export const forceRefresh = () => {
    return {
        type: GET_CRYPTO,
        loading: false,
    };
};

export const toggleFavorite = (data, wasFavorite) => {
    return {
        type: TOGGLE_FAVORITE,
        payload: data,
        wasFavorite,
        loading: false,
    };
};

export const fetchDataFulfilled = (data) => {
    return {
        type: GET_CRYPTO_SUCCESS,
        payload: data,
        loading: false,
    };
};

export const fetchDataRejected = (error) => {
    return {
        type: GET_CRYPTO_FAIL,
        payload: error,
        loading: false,
    };
};
