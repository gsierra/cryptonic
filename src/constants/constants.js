export const GRAPH_HEIGHT = 200;
export const GRAPH_SEPARATION_LINES = 10;
export const PAGINATION_BATCH = 10;
