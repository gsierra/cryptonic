import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
/*
import reducer from '../Reducers';
*/
import cryptoReducer, {fetchData, getNextBatch, fetchDataFulfilled, fetchDataRejected} from '../Reducers/cryptoReducer';
import {dailyStats, fetchCrypto, fetchTickers} from "../middleware/api";
/*import {persistReducer} from "redux-persist/es/persistReducer";
import {persistStore} from "redux-persist/es/persistStore";
import storage from "redux-persist/es/storage";*/

export const getSortedCrypto = (sorted, justTop) => {
    return async dispatch => {
        try {
            /*An alternative toggle button have been disabled...*/
            const cryptoListObj =/* timeStats ? await fetchTickers() :*/ await dailyStats();
/*
            console.log("TEST: 1. Obj: "+JSON.stringify(cryptoListObj));
*/
            dispatch(fetchData(true));

            /**2. Convert it into Array in order to Sort
             *  and display in a FlatList rather than Object.map()*/
            const pricesArray = [];
            Object.keys(cryptoListObj).forEach((key)=>{
               pricesArray.push({
                       label: key,
                       symbol: cryptoListObj[key].symbol, //redundant
                       value: cryptoListObj[key].bidPrice,
                       priceChange: cryptoListObj[key].priceChange,
                       priceChangePercent: cryptoListObj[key].priceChangePercent,
                       bidQty: cryptoListObj[key].bidQty,
                       askPrice: cryptoListObj[key].askPrice,
                       askQty: cryptoListObj[key].askQty,
                       lastPrice: cryptoListObj[key].lastPrice,
                       isFavorite: false,
                   })
            });
/*
            console.log("TEST: 2. Obj: "+JSON.stringify(pricesArray));
*/

            /**3. Retrieved dispatch an action altering redux state
             * depending on sorted status*/
            const sortedArr = sorted ?
                pricesArray.sort((a, b) =>
                    b.priceChangePercent - a.priceChangePercent) :
                pricesArray;

            dispatch(fetchDataFulfilled( justTop? sortedArr.slice(0,10) : sortedArr));

            /*dispatch(getNextBatch());*/
/*
            console.log("TEST: 3. Dispatched: "+JSON.stringify(sortedArr));
*/

        } catch(error) {
            console.log('----------Crypto Error---------', error);
            dispatch(fetchDataRejected(error))
        }
    }
};

export const getNextSortedCrypto = () => {
/*
    console.log("End reached, loading new batch...");
*/
    return dispatch => {
        dispatch(getNextBatch());
    }
};

export const toggleFavorite = (item, wasFavorite) => {
    return dispatch => {
        dispatch(toggleFavorite(item, wasFavorite));
    }
};
/*
const persistConfig = {
    key: 'root',
    storage: storage,
};
const pReducer = persistReducer(persistConfig, cryptoReducer);
*/
const store = createStore(cryptoReducer, applyMiddleware(thunk));
/*
export const persistor = persistStore(store);
*/
export default store;
