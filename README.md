# Cryptonic
Test Repo for [Holded](https://jobs.holded.com/)

# Instructions

CryptoCurrency app in React-Native involving:
react-navigation, Redux, Thunk as a middleware.

![holded-test|small](https://i.imgur.com/DFztNpo.png=60x)
![holded-test|small](https://i.imgur.com/Ccmsqj4.png=60x)

Setup

clone the repo
```
https://gitlab.com/gsierra/cryptonic

cd cryptonic

npm install
```

Run the app

```
npm start
```

For Android

```
react-native run-android
```

For iOS

```
cd ios & pod install
cd .. && react-native run-ios
```

Considerations:

## Extra added features
- Filter
- Chart with zoom
- Iphone X responsiveness

![holded-test|small](https://i.imgur.com/lS3uh8f.png=60x)

## Nice to have  / Improvements
- Might be implemented zoom wiht more detail and usecases for the graph
- Redux Persistance / AsyncSTorage for favorite list persisitng in cache memory
- NetInfo moved to Redux store
- Add pagination  to improve loading time:
```
onEndReached={()=>{/*Todo implement pagination*/}}

```
